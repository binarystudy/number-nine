function post({ id }, _, context) {
  return context.prisma.comment({ id }).post();
}

function commentReactions({ id }, _, context) {
  return context.prisma.comment({ id }).commentReactions();
}

module.exports = {
  post,
  commentReactions,
};
