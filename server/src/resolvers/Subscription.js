function newPostSubscribe(_, _, context) {
  return context.prisma.$subscribe
    .post({
      mutation_in: ['CREATED'],
    })
    .node();
}

const newPost = {
  subscribe: newPostSubscribe,
  resolve: payload => payload,
};

function newPostReactionSubscribe(_, _, context) {
  return context.prisma.$subscribe
    .postReaction({
      mutation_in: ['CREATED'],
    })
    .node();
}

const newPostReaction = {
  subscribe: newPostReactionSubscribe,
  resolve: payload => payload,
};

function newCommentSubscribe(_, _, context) {
  return context.prisma.$subscribe
    .comment({
      mutation_in: ['CREATED'],
    })
    .node();
}

const newComment = {
  subscribe: newCommentSubscribe,
  resolve: payload => payload,
};

function newCommentReactionSubscribe(_, _, context) {
  return context.prisma.$subscribe
    .commentReaction({
      mutation_in: ['CREATED'],
    })
    .node();
}

const newCommentReaction = {
  subscribe: newCommentReactionSubscribe,
  resolve: payload => payload,
};

module.exports = {
  newPost,
  newPostReaction,
  newComment,
  newCommentReaction,
};
