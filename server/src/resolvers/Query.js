function posts(_, { filter, skip, first, orderBy }, context) {
  const where = filter
    ? {
        content_contains: filter,
      }
    : {};
  const postList = context.prisma.posts({
    where,
    skip,
    first,
    orderBy,
  });
  const count = context.prisma
    .postsConnection({
      where,
    })
    .aggregate()
    .count();
  return {
    postList,
    count,
  };
}

function comments(_, args, context) {
  const comments = context.prisma.comments();
  return comments;
}

module.exports = {
  posts,
  comments,
  // postReaction,
};
