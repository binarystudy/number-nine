function post({ id }, _, context) {
  return context.prisma.postReaction({ id }).post();
}

module.exports = {
  post,
};
