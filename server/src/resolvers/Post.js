function comments({ id }, _, context) {
  return context.prisma.post({ id }).comments();
}

function postReactions({ id }, _, context) {
  return context.prisma.post({ id }).postReactions();
}

module.exports = {
  comments,
  postReactions,
};
