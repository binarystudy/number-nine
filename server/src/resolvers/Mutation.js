function createPost(_, { content }, context) {
  return context.prisma.createPost({
    content,
  });
}

function createComment(_, { content, postId }, context) {
  const post = context.prisma.$exists.post({ id: postId });
  if (!post) {
    throw new Error(`Post with id ${postId} does not exist`);
  }
  return context.prisma.createComment({
    content,
    post: { connect: { id: postId } },
  });
}

function createPostReaction(_, { isLike, postId }, context) {
  const post = context.prisma.$exists.post({ id: postId });
  if (!post) {
    throw new Error(`Post with id ${postId} does not exist`);
  }
  return context.prisma.createPostReaction({
    isLike,
    post: { connect: { id: postId } },
  });
}

function createCommentReaction(_, { isLike, commentId }, context) {
  const post = context.prisma.$exists.comment({ id: commentId });
  if (!post) {
    throw new Error(`Comment with id ${commentId} does not exist`);
  }
  return context.prisma.createCommentReaction({
    isLike,
    comment: { connect: { id: commentId } },
  });
}

module.exports = {
  createPost,
  createComment,
  createPostReaction,
  createCommentReaction,
};
