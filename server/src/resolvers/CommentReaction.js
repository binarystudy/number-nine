function comment({ id }, _, context) {
  return context.prisma.commentReaction({ id }).comment();
}

module.exports = {
  comment,
};
