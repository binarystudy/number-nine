const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require('./generated/prisma-client');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Post = require('./resolvers/Post');
const Comment = require('./resolvers/Comment');
const Subscription = require('./resolvers/Subscription');
const PostReaction = require('./resolvers/PostReaction');
const CommentReaction = require('./resolvers/CommentReaction');

const resolvers = {
  Query,
  Mutation,
  Post,
  Comment,
  Subscription,
  PostReaction,
  CommentReaction,
};

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: {
    prisma,
  },
});
server.start(() => console.log('Server is running on localhost:4000'));
