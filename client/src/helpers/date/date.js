export const date = (locale = 'en-GB') => {
  const formatter = (date, options) => {
    if (!date.length) {
      return '';
    }
    return new Intl.DateTimeFormat(locale, options).format(new Date(date));
  };

  const fullDate = date => {
    const options = {
      year: 'numeric',
      day: '2-digit',
      month: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    };
    const dateResult = formatter(date, options);
    return dateResult.replace(/\//g, '.').replace(',', '');
  };

  return { fullDate };
};

export const intlDate = date();
