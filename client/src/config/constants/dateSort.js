export const DateSort = {
  ASC: 'createdAt_ASC',
  DESC: 'createdAt_DESC',
};
