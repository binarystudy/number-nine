import { gql } from '@apollo/client';

export const POST_QUERY = gql`
  query PostQuery($filter: String, $skip: Int, $first: Int, $orderBy: PostOrderByInput) {
    posts(filter: $filter, skip: $skip, first: $first, orderBy: $orderBy) {
      postList {
        id
        content
        createdAt
        comments {
          id
          content
          createdAt
          commentReactions {
            id
            isLike
          }
          post {
            id
          }
        }
        postReactions {
          id
          isLike
        }
      }
      count
    }
  }
`;
