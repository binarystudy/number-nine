import { gql } from '@apollo/client';

export const CREATE_POST_MUTATION = gql`
  mutation CreatePost($content: String!) {
    createPost(content: $content) {
      id
      content
      createdAt
      comments {
        id
        content
        createdAt
      }
      postReactions {
        isLike
      }
    }
  }
`;

export const CREATE_COMMENT_MUTATION = gql`
  mutation CreateComment($content: String!, $postId: ID!) {
    createComment(content: $content, postId: $postId) {
      id
      content
      createdAt
      post {
        id
      }
    }
  }
`;

export const CREATE_POST_REACTION_MUTATION = gql`
  mutation CreatePostReaction($isLike: Boolean!, $postId: ID!) {
    createPostReaction(isLike: $isLike, postId: $postId) {
      isLike
    }
  }
`;

export const CREATE_COMMENT_REACTION_MUTATION = gql`
  mutation CreateCommentReaction($isLike: Boolean!, $commentId: ID!) {
    createCommentReaction(isLike: $isLike, commentId: $commentId) {
      isLike
    }
  }
`;
