import { gql } from '@apollo/client';

export const NEW_POSTS_SUBSCRIPTION = gql`
  subscription {
    newPost {
      id
      content
      createdAt
      comments {
        id
        content
        createdAt
      }
      postReactions {
        isLike
      }
    }
  }
`;

export const NEW_COMMENTS_SUBSCRIPTION = gql`
  subscription {
    newComment {
      id
      content
      createdAt
      post {
        id
      }
      commentReactions {
        isLike
      }
    }
  }
`;

export const NEW_POST_REACTION_SUBSCRIPTION = gql`
  subscription {
    newPostReaction {
      id
      isLike
      post {
        id
      }
    }
  }
`;

export const NEW_COMMENT_REACTION_SUBSCRIPTION = gql`
  subscription {
    newCommentReaction {
      isLike
			comment {
				id
				post {
					id
				}
			}
    }
  }
`;
