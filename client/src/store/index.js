import { configureStore, combineReducers } from '@reduxjs/toolkit';

import { navigationReducers } from './navigation.reducers';

const reducer = combineReducers({
  navigation: navigationReducers,
});

export const store = configureStore({ reducer });
export * from './navigation.reducers';
