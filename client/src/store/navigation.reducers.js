import { createAction, createReducer } from '@reduxjs/toolkit';
import { PostOnPage } from '../config';

export const changeSort = createAction('navigation/sort');
export const changeFilter = createAction('navigation/filter');
export const changePage = createAction('navigation/page');

const initialState = {
  skip: 0,
  first: PostOnPage,
};

export const navigationReducers = createReducer(initialState, builder => {
  builder
    .addCase(changeSort, (state, action) => {
      if (!action.payload) {
        delete state.orderBy;
      } else {
        state.orderBy = action.payload;
      }
    })
    .addCase(changeFilter, (state, action) => {
      if (!action.payload) {
        delete state.filter;
      } else {
        state.filter = action.payload;
      }
    })
    .addCase(changePage, (state, action) => {
      state.skip = action.payload;
    });
});
