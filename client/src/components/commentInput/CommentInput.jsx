import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useMutation } from '@apollo/client';
import { CREATE_COMMENT_MUTATION, POST_QUERY } from '../../config';
import { Loader } from '../common';

import './styles.scss';

export const CommentInput = props => {
  const { onClose, id } = props;
  const [disabled, setDisabled] = useState(true);
  const { navigation } = useSelector(state => state);
  const [value, setValue] = useState('');
  const [createPost, { loading }] = useMutation(CREATE_COMMENT_MUTATION, {
    update(cache, { data: { createComment } }) {
      const data = cache.readQuery({ query: POST_QUERY, variables: navigation });
      const posts = data.posts.postList.map(post =>
        post.id !== id ? post : [...post.comments, createComment]
      );
      cache.writeQuery({
        query: POST_QUERY,
        variables: navigation,
        data: {
          posts: {
            ...data.posts,
            postList: posts,
          },
        },
      });
    },
    onCompleted: () => {
      onClose();
    },
  });
  const handleClick = () => {
    const trimValue = value.trim();
    if (!trimValue.length) {
      return;
    }
    createPost({ variables: { content: trimValue, postId: id } });
    setValue('');
    setDisabled(true);
  };
  const handleChange = event => {
    const value = event.target.value;
    setValue(value);
    setDisabled(!value.length);
  };
  return (
    <div className="comment-input">
      {loading && <Loader />}
      <textarea value={value} name="comment" onChange={handleChange} className="comment-input-text">
        {value}
      </textarea>
      <div className="button-group">
        <button
          className="btn"
          onClick={() => {
            onClose();
          }}
        >
          x
        </button>
        <button
          disabled={disabled}
          onClick={handleClick}
          className={`btn ${!disabled ? ' btn-success' : ''}`}
        >
          Send
        </button>
      </div>
    </div>
  );
};
