import React from 'react';
import { useMutation } from '@apollo/client';
import { useSelector } from 'react-redux';
import { CREATE_COMMENT_REACTION_MUTATION, POST_QUERY } from '../../config';
import { Loader } from '../common';

export const CommentReaction = props => {
  const { commentReactions, id, postId } = props;
  const { navigation } = useSelector(state => state);
  const [createReaction, { loading }] = useMutation(CREATE_COMMENT_REACTION_MUTATION, {
    update(cache, { data: { createCommentReaction } }) {
      const data = cache.readQuery({ query: POST_QUERY, variables: navigation });
      const posts = data.posts.postList.map(post => {
        if (post.id !== postId) {
          return post;
        }
        return post.comments.map(comment =>
          comment.id !== id ? comment : [...comment.commentReactions, createCommentReaction]
        );
      });
      cache.writeQuery({
        query: POST_QUERY,
        data: {
          posts: {
            ...data.posts,
            postList: posts,
          },
        },
      });
    },
  });

  const likeCount = commentReactions.filter(reaction => reaction.isLike).length;
  const disLikeCount = commentReactions.length - likeCount;

  const handleLike = isLike => {
    createReaction({ variables: { isLike, commentId: id } });
  };

  return (
    <div className="comment-reaction">
      {loading && <Loader />}
      <span className="reaction-like">
        <i className="fas fa-thumbs-up" onClick={() => handleLike(true)}></i>
        {Boolean(likeCount) ? <span>{likeCount}</span> : null}
      </span>
      <span className="reaction-dislike">
        <i className="fas fa-thumbs-down" onClick={() => handleLike(false)}></i>
        {Boolean(disLikeCount) ? <span>{disLikeCount}</span> : null}
      </span>
    </div>
  );
};
