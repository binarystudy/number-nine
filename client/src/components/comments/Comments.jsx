import React from 'react';
import { CommentReaction } from './CommentReaction';
import { getNumber } from '../../helpers';

import './styles.scss';

export const Comment = ({ comment, postId }) => {
  return (
    <div className="comment-root">
      <div className="comment-header">
        <div>{comment.content}</div>
        <div>{getNumber(comment.id)}</div>
      </div>
      <div className="comment-footer">
        <CommentReaction
          commentReactions={comment.commentReactions}
          id={comment.id}
          postId={postId}
        />
      </div>
    </div>
  );
};
