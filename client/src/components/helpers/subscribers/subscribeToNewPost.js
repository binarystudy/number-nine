import { NEW_POSTS_SUBSCRIPTION } from '../../../config';

export const subscribeToNewPost = subscribeToMore => {
  subscribeToMore({
    document: NEW_POSTS_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) {
        return prev;
      }
      const { newPost } = subscriptionData.data;
      const exist = prev.posts.postList.find(({ id }) => id === newPost.id);
      if (exist) {
        return;
      }
      return {
        ...prev,
        posts: {
          ...prev.posts,
          postList: [...prev.posts.postList, newPost],
          count: prev.posts.postList.count + 1,
        },
      };
    },
  });
};
