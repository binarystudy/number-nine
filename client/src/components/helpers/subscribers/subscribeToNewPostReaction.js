import { NEW_POST_REACTION_SUBSCRIPTION } from '../../../config';

export const subscribeToNewPostReaction = subscribeToMore => {
  subscribeToMore({
    document: NEW_POST_REACTION_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) {
        return prev;
      }
      const { newPostReaction } = subscriptionData.data;
      const prevPost = prev.posts.postList.find(({ id }) => id === newPostReaction.post.id);
      if (!prevPost) {
        return;
      }
      const exist = prevPost.postReactions.find(({ id }) => id === newPostReaction.id);
      if (exist) {
        return;
      }
      const postWithNewReaction = {
        ...prevPost,
        postReactions: [...prevPost.postReactions, newPostReaction],
      };
      return {
        ...prev,
        posts: {
          ...prev.posts,
          postList: prev.posts.postList.map(post =>
            post.id !== postWithNewReaction.id ? post : postWithNewReaction
          ),
        },
      };
    },
  });
};
