import { NEW_COMMENT_REACTION_SUBSCRIPTION } from '../../../config';

export const subscribeToNewCommentReact = subscribeToMore => {
  subscribeToMore({
    document: NEW_COMMENT_REACTION_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) {
        return prev;
      }
      const { newCommentReaction: reaction } = subscriptionData.data;
      const prevPost = prev.posts.postList.find(({ id }) => id === reaction.comment.post.id);
      if (!prevPost) {
        return;
      }
      const prevComment = prevPost.comments.find(({ id }) => id === reaction.comment.id);
      if (!prevComment) {
        return;
      }
      const exist = prevComment.commentReactions.find(({ id }) => id === reaction.id);
      if (exist) {
        return;
      }
      const newComment = {
        ...prevComment,
        commentReactions: [...prevComment.commentReactions, reaction],
      };
      const newPost = {
        ...prevPost,
        comments: prevPost.comments.map(comment =>
          comment.id !== newComment.id ? comment : newComment
        ),
      };
      return {
        ...prev,
        posts: {
          ...prev.posts,
          postList: prev.posts.postList.map(post => (post.id !== newPost.id ? post : newPost)),
        },
      };
    },
  });
};
