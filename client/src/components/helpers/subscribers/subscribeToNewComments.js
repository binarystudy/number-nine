import { NEW_COMMENTS_SUBSCRIPTION } from '../../../config';

export const subscribeToNewComments = subscribeToMore => {
  subscribeToMore({
    document: NEW_COMMENTS_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) {
        return prev;
      }
      const { newComment } = subscriptionData.data;
      const prevPost = prev.posts.postList.find(({ id }) => id === newComment.post.id);
      if (!prevPost) {
        return;
      }
      const exist = prevPost.comments.find(({ id }) => id === newComment.id);
      if (exist) {
        return;
      }
      const postWithNewComment = { ...prevPost, comments: [...prevPost.comments, newComment] };
      return {
        ...prev,
        posts: {
          ...prev.posts,
          postList: prev.posts.postList.map(post =>
            post.id !== postWithNewComment.id ? post : postWithNewComment
          ),
        },
      };
    },
  });
};
