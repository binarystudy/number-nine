import React from 'react';
import { useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { PostList } from './PostList';
import { Loader, Error } from './common';
import {
  subscribeToNewPost,
  subscribeToNewComments,
  subscribeToNewPostReaction,
  subscribeToNewCommentReact,
} from './helpers';
import { POST_QUERY } from '../config';

export const Posts = () => {
  const { navigation } = useSelector(state => state);
  const { subscribeToMore, loading, error, ...result } = useQuery(POST_QUERY, {
    variables: navigation,
  });

  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <Error />;
  }
  return (
    <PostList
      {...result}
      subscribeToNewPost={() => {
        subscribeToNewPost(subscribeToMore);
        subscribeToNewComments(subscribeToMore);
        subscribeToNewPostReaction(subscribeToMore);
        subscribeToNewCommentReact(subscribeToMore);
      }}
    />
  );
};
