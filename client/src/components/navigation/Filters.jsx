import React, { useState } from 'react';

export const Filters = ({ onChange }) => {
  const [filter, setFilter] = useState('');
  const [disabled, setDisabled] = useState(true);
  const handleFilter = event => {
    const value = event.target.value;
    setFilter(value);
    setDisabled(!value.length);
  };
  const handleOnFilter = () => {
    const trimFilter = filter.trim();
    if (!trimFilter.length) {
      return;
    }
    onChange(filter);
    setFilter('');
    setDisabled(true);
  };
  return (
    <div className="filter-root">
      <input value={filter} type="text" name="filter" onChange={handleFilter} />
      <button className="btn" onClick={handleOnFilter} disabled={disabled}>
        <i className="fas fa-filter"></i>
      </button>
    </div>
  );
};
