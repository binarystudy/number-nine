import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Filters } from './Filters';
import { Sort } from './Sort';
import { DateSort } from '../../config';
import { changeSort, changeFilter } from '../../store';

import './styles.scss';

const labelSort = {
  [DateSort.ASC]: 'ascend',
  [DateSort.DESC]: 'descend',
};

export const Navigation = ({ onChange }) => {
  const { navigation } = useSelector(state => state);
  const dispatch = useDispatch();
  const handleChangeFilters = filter => {
    dispatch(changeFilter(filter));
  };
  const handleChangeSort = orderBy => {
    dispatch(changeSort(orderBy));
  };
  return (
    <div className="navigation-root">
      <div className="navigation-header">
        <Filters onChange={handleChangeFilters} />
        <Sort onChange={handleChangeSort} sort={navigation.orderBy} />
      </div>
      {!navigation.filter && !navigation.orderBy ? null : (
        <div className="navigation-footer">
          {navigation.filter ? (
            <div className="navigation-filter">
              <p>{navigation.filter}</p>
              <button className="btn" onClick={() => handleChangeFilters()}>
                clear
              </button>
            </div>
          ) : null}
          {navigation.orderBy ? (
            <div className="navigation-sort">
              <p>{labelSort[navigation.orderBy]}</p>
              <button className="btn" onClick={() => handleChangeSort()}>
                clear
              </button>
            </div>
          ) : null}
        </div>
      )}
    </div>
  );
};
