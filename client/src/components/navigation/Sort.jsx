import React, { useState } from 'react';
import { DateSort } from '../../config';

export const Sort = ({ onChange, sort = '' }) => {
  const [isSort, setIsSort] = useState(sort);
  const handleOnSort = value => {
    onChange(value);
    setIsSort(value);
  };
  return (
    <div className="sort-root">
      <button
        className="btn"
        onClick={() => handleOnSort(DateSort.ASC)}
        disabled={isSort === DateSort.ASC}
      >
        <i className="fas fa-sort-amount-up"></i>
      </button>
      <button
        className="btn"
        onClick={() => handleOnSort(DateSort.DESC)}
        disabled={isSort === DateSort.DESC}
      >
        <i className="fas fa-sort-amount-down"></i>
      </button>
    </div>
  );
};
