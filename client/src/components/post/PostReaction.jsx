import React from 'react';
import { useMutation } from '@apollo/client';
import { useSelector } from 'react-redux';
import { Loader } from '../common';
import { CREATE_POST_REACTION_MUTATION, POST_QUERY } from '../../config';

export const PostReaction = props => {
  const { postReactions, id } = props;
  const { navigation } = useSelector(state => state);
  const [createReaction, { loading }] = useMutation(CREATE_POST_REACTION_MUTATION, {
    update(cache, { data: { createPostReaction } }) {
      const data = cache.readQuery({ query: POST_QUERY, variables: navigation });
      const posts = data.posts.postList.map(post =>
        post.id !== id ? post : [...post.postReactions, createPostReaction]
      );
      cache.writeQuery({
        query: POST_QUERY,
        data: {
          posts: {
            ...data.posts,
            postList: posts,
          },
        },
      });
    },
  });

  const likeCount = postReactions.filter(reaction => reaction.isLike).length;
  const disLikeCount = postReactions.length - likeCount;

  const handleLike = () => {
    createReaction({ variables: { isLike: true, postId: id } });
  };

  const handleDislike = () => {
    createReaction({ variables: { isLike: false, postId: id } });
  };

  return (
    <div className="post-reaction">
      {loading && <Loader />}
      <span className="reaction-like">
        <i className="fas fa-thumbs-up" onClick={handleLike}></i>
        {Boolean(likeCount) ? <span>{likeCount}</span> : null}
      </span>
      <span className="reaction-dislike">
        <i className="fas fa-thumbs-down" onClick={handleDislike}></i>
        {Boolean(disLikeCount) ? <span>{disLikeCount}</span> : null}
      </span>
    </div>
  );
};
