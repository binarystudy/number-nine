import React, { useState } from 'react';
import { PostReaction } from './PostReaction';
import { Comment } from '../comments';
import { getNumber, intlDate } from '../../helpers';
import { CommentInput } from '../commentInput';

import './styles.scss';

export const Post = props => {
  const { comments, content, postReactions, createdAt, id } = props;
  const [reply, onReply] = useState(false);
  return (
    <div className="post-root">
      <div className="post-content">
        <span>{content}</span>
        <div className="wrapper-time">
          <span>{getNumber(id)}</span>
          <span className="post-time">{intlDate.fullDate(createdAt)}</span>
        </div>
      </div>
      <div className="post-footer">
        <PostReaction postReactions={postReactions} id={id} />
        {!reply && (
          <button className="btn btn-light" onClick={() => onReply(true)}>
            Reply
          </button>
        )}
      </div>
      {reply && <CommentInput id={id} onClose={() => onReply(false)} />}
      {comments.length ? (
        <div className="comments-list">
          {comments.map(comment => (
            <Comment key={comment.id} comment={comment} postId={id} />
          ))}
        </div>
      ) : null}
    </div>
  );
};
