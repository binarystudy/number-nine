import React from 'react';
import './loader.scss';

export const Loader = () => {
  return (
    <div className="wrapper-preloader">
      <div className="preloader"></div>
    </div>
  );
};
