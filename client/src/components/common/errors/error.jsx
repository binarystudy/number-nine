import React from 'react';

import './error.scss';

export const Error = () => {
  return <div className="error-root">Something Wrong</div>;
};
