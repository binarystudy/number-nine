import React, { useEffect } from 'react';
import { Post } from './post';
import { PostInput } from './postInput';
import { Navigation } from './navigation';
import { Paginate } from './paginate';

import './styles.scss';

export const PostList = props => {
  const { data, subscribeToNewPost } = props;
  useEffect(() => {
    subscribeToNewPost();
  }, [subscribeToNewPost]);
  return (
    <div className="post-list">
      <Navigation />
      {data.posts.postList.map(post => (
        <Post key={post.id} {...post} />
      ))}
      <PostInput />
      <Paginate count={data.posts.count} />
    </div>
  );
};
