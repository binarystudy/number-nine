import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { changePage } from '../../store';

import './styles.scss';

export const Paginate = ({ count }) => {
  const { navigation } = useSelector(state => state);
  const dispatch = useDispatch();
  const pages = Math.ceil(count / navigation.first);
  const page = navigation.skip / navigation.first;
  const handleClick = value => {
    const newPage = page + value;
    if (newPage < 0 || newPage >= pages) {
      return;
    }
    dispatch(changePage(newPage * navigation.first));
  };
  return (
    <div className="paginate-root">
      <div className="paginate-arrows">
        <button className="btn" disabled={page === 0} onClick={() => handleClick(-1)}>
          <i className="fas fa-arrow-circle-left"></i>
        </button>
        <button className="btn" disabled={pages <= page + 1} onClick={() => handleClick(1)}>
          <i className="fas fa-arrow-circle-right"></i>
        </button>
      </div>
      <div>
        <span>{page + 1}</span> page from <span>{pages}</span>
      </div>
    </div>
  );
};
