import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useMutation } from '@apollo/client';
import { Loader } from '../common';
import { CREATE_POST_MUTATION, POST_QUERY } from '../../config';

import './styles.scss';

export const PostInput = () => {
  const [disabled, setDisabled] = useState(true);
  const { navigation } = useSelector(state => state);
  const [value, setValue] = useState('');
  const [createPost, { loading }] = useMutation(CREATE_POST_MUTATION, {
    update(cache, { data: { createPost } }) {
      const data = cache.readQuery({ query: POST_QUERY, variables: navigation });
      const posts = [...data.posts.postList, createPost];
      cache.writeQuery({
        query: POST_QUERY,
        variables: navigation,
        data: {
          posts: {
            ...data.posts,
            postList: posts,
            count: data.posts.count + 1,
          },
        },
      });
    },
  });
  const handleClick = () => {
    const trimValue = value.trim();
    if (!trimValue.length) {
      return;
    }
    createPost({ variables: { content: trimValue } });
    setValue('');
    setDisabled(true);
  };
  const handleChange = event => {
    const value = event.target.value;
    setValue(value);
    setDisabled(!value.length);
  };
  return (
    <div className="post-input">
      {loading && <Loader />}
      <textarea value={value} name="post" onChange={handleChange} className="post-input-text">
        {value}
      </textarea>
      <button
        disabled={disabled}
        onClick={handleClick}
        className={`btn ${!disabled ? ' btn-success' : ''}`}
      >
        Send
      </button>
    </div>
  );
};
